//
//  Services.swift
//  Podcasts
//
//  Created by Long Vu on 5/29/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import Alamofire
import FeedKit

class APIService {
    
    // MARK: - Properties
    let baseiTunesSearchURL = "https://itunes.apple.com/search"
    
    // Singleton
    static let shared = APIService()
    
    private init () {}
    
    // MARK: - Method
    func fetchPodcasts(_ searchText: String, completionHandler: @escaping ([Podcast]) -> ()) {
        print("Searching for podcasts with text: ", searchText)
        
        let parameters = [
            "term": searchText,
            "media": "podcast"
        ]
        
        Alamofire.request(self.baseiTunesSearchURL, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (dataResponse) in
            if let errResponse = dataResponse.error {
                print("Error", errResponse)
                completionHandler([])
                return
            }
            
            if let data = dataResponse.data {
                //                print(utf8Data)
                do {
                    let decoder = JSONDecoder()
                    let searchResults = try decoder.decode(SearchResults.self, from: data)
                    
                    let results = searchResults.results
                    
                    var podcasts = [Podcast]()
                    
                    results.forEach({ podcast in
                        podcasts.append(podcast)
                    })
                    
                    completionHandler(podcasts)
                    
                }
                catch let errDecode {
                    print("Fail Decoding Data", errDecode)
                    completionHandler([])
                    return
                }
            }
        }
    }
    
    func fetchEpisodes(_ feedUrl: String, completionHandler: @escaping ([Episode]) -> ()) {
        print("fetching episode with feedURL: ", feedUrl)
        
        guard let url = URL(string: feedUrl) else {
            print("URL invalid")
            completionHandler([])
            return
        }
        
        DispatchQueue.global(qos: .background).async {
            let feedParser = FeedParser(URL: url)
            
            feedParser.parseAsync { result in
                if result.isFailure, let err = result.error {
                    print("Parsing Feed Error: ", err)
                    completionHandler([])
                    return
                }
                
                guard let feed = result.rssFeed else {return}
                
                var episodes = [Episode]()
                
                feed.items?.forEach({ item in
                    var episode = Episode(item)
                    episode.imageURL = feed.iTunes?.iTunesImage?.attributes?.href
                    episodes.append(episode)
                })
                
                completionHandler(episodes)
            }
        }
    }
    
    func downloadEpisode(_ episode: Episode) {
        print("Downloading episode with Alamofire")
        let url = episode.streamUrl
        
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        Alamofire
        .download(url, to: destination)
        .downloadProgress { progress in
            print(progress.fractionCompleted)
            NotificationCenter.default.post(name: .didBeginDownload, object: nil, userInfo: [
                "episode": episode,
                "progress": Float(progress.fractionCompleted)
                ])
        }
        .response {
            resp in
            let destination = resp.destinationURL?.absoluteString
            
            var downloadedEpisodes = userDefault.downloadedEpisodes()
            
            let idx = downloadedEpisodes.firstIndex(where: {
                $0.title == episode.title && $0.author == episode.author
            }) ?? -1
            
            if idx != -1 {
                downloadedEpisodes[idx].fileUrl = destination
                
                let encoder = JSONEncoder()
                do {
                    let data = try encoder.encode(downloadedEpisodes)
                    userDefault.set(data, forKey: downloadEpisodeKey)
                }
                catch let err {
                    print("Faild to encode: ", err)
                }
            }
        }
    }
    
    struct SearchResults: Codable {
        let resultCount: Int
        let results: [Podcast]
    }
}
