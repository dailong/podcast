//
//  PodcastCell.swift
//  Podcasts
//
//  Created by Dai Long on 11/21/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import SDWebImage

class PodcastCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var artWork: UIImageView!
    @IBOutlet weak var collectionName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var numberEpisodes: UILabel!
    
    // MARK: - Properties
    var podcast: Podcast! {
        didSet {
            self.collectionName.text = podcast.collectionName
            self.artistName.text = podcast.artistName
            self.numberEpisodes.text = String(podcast.trackCount) + " episodes"
            
            self.artWork.sd_setImage(with: URL(string: podcast.artworkUrl100!), completed: nil)
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.artWork.image = #imageLiteral(resourceName: "podcasts-icon")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
