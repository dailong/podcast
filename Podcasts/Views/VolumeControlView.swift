//
//  VolumeControlView.swift
//  Podcasts
//
//  Created by Dai Long on 12/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import MediaPlayer

class VolumeControlView: MPVolumeView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        // Initializer
        initializer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // Initializer
        initializer()
    }
    
    private func initializer() {
        isHidden = false
    }

}
