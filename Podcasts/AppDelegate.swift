//
//  AppDelegate.swift
//  Podcasts
//
//  Created by Dai Long on 11/20/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        
//        application.statusBarStyle = .lightContent
        // Override point for customization after application launch.
        self.window = UIWindow()
        self.window?.makeKeyAndVisible()
        
        self.window?.rootViewController = MainTabBarController()
        
        NotificationCenter.default.addObserver(self, selector: #selector(volumeDidChange(notification:)), name: Notification.Name.systemVolumeDidChange, object: nil)
        
        return true
    }

    @objc func volumeDidChange(notification: NSNotification) {
        let volume = notification.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as! Float
        print("Volume: ", volume)
//        AVPlayerControl.shared.adjustVolume(volume)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        NotificationCenter.default.removeObserver(self)
    }


}

