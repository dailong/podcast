//
//  AVPlayerControl.swift
//  Podcasts
//
//  Created by Dai Long on 12/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import AVKit
import UIKit
import MediaPlayer

protocol AVPlayerControlDelegate: class {
    func observePlayerCurrentTime(_ time: CMTime)
}

// Design Pattern: - Singleton
class AVPlayerControl: NSObject {
    
    static var shared = AVPlayerControl()
    
    var avPlayer: AVPlayer = {
        let avPlayer = AVPlayer()
        avPlayer.automaticallyWaitsToMinimizeStalling = false
        return avPlayer
    }()
    
    private var avPlayerItem: AVPlayerItem?
    
    weak var delegate: AVPlayerControlDelegate?
    
    public var durationTime: CMTime {
        get {
            return self.avPlayer.currentItem?.duration ?? CMTimeMake(value: 1, timescale: 1)
        }
    }
    
    public var currentTime: CMTime {
        get {
            return self.avPlayer.currentTime()
        }
    }
    
    var volume: Float = 1.0
    
    // Singleton PATTERN
    // MARK: - Initializer
    private override init() {
        super.init()
        
    }
    
    func observePlayerCurrentTime() {
        let interval = CMTimeMake(value: 1, timescale: 2)
        self.avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: .main) { [weak self] time in
            self?.delegate?.observePlayerCurrentTime(time)
            
            // Update Playback Time in Remote Control Center
            self?.updateElapsedPlayBackTime(time)
            self?.updatePlaybackDuration(self!.durationTime)
        }
    }
    
    // Setup session allow play in background
    fileprivate func setupAudioSession() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .allowAirPlay)
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch let sesstionErr {
            print(sesstionErr.localizedDescription)
        }
    }
    
    fileprivate func setupRemoteControl() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        let remoteCommand = MPRemoteCommandCenter.shared()
        
        // Play
        remoteCommand.playCommand.isEnabled = true
        remoteCommand.playCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.play()
//            MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 1.0
            return .success
        }
        
        // Pause
        remoteCommand.pauseCommand.isEnabled = true
        remoteCommand.pauseCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.play()
//            MPNowPlayingInfoCenter.default().nowPlayingInfo?[MPNowPlayingInfoPropertyPlaybackRate] = 0
            return .success
        }
        
        // For Headphone
        remoteCommand.togglePlayPauseCommand.isEnabled = true
        remoteCommand.togglePlayPauseCommand.addTarget { (_) -> MPRemoteCommandHandlerStatus in
            self.play()
            return .success
        }
        
        // Skip Forward Command
        remoteCommand.skipForwardCommand.isEnabled = true
        remoteCommand.skipForwardCommand.addTarget(self, action: #selector(handleSkipForwardAction))
        
        // Skip Backward Command
        remoteCommand.skipBackwardCommand.isEnabled = true
        remoteCommand.skipBackwardCommand.addTarget(self, action: #selector(handleSkipBackwardAction))
        
        //
        remoteCommand.changePlaybackPositionCommand.isEnabled = true
        
        remoteCommand.changePlaybackPositionCommand.addTarget { [weak self] event -> MPRemoteCommandHandlerStatus in
            guard let evt = event as? MPChangePlaybackPositionCommandEvent else {fatalError("CAN'T DOWNCAST")}
            let time = evt.positionTime
            
            let timeToChanged = CMTime(seconds: time, preferredTimescale: 1000000)
            
            let group = DispatchGroup()
            
            group.enter()
            
            DispatchQueue.global(qos: .default).async {
                self?.avPlayer.seek(to: timeToChanged)
                group.leave()
            }
            
            group.wait()
            
//            let operationQueue = OperationQueue()

//            let seekBlock = BlockOperation(block: {
//                self?.avPlayer.seek(to: timeToChanged, completionHandler: { (_) in
//                    print("DONEDONE")
//                })
//            })
//
//            operationQueue.addOperations([seekBlock], waitUntilFinished: true)
            
            return .success
        }
    }
    
    @objc private func changedPlaybackPositionCommand(_ event: MPChangePlaybackPositionCommandEvent) {
        print("change")
    }
    
    @objc private func handleSkipForwardAction() {
        self.seekToCurrentTime(delta: 15)
    }
    
    @objc private func handleSkipBackwardAction() {
        self.seekToCurrentTime(delta: -15)
    }

    
    // Update Elapsed Playback Time
    fileprivate func updateElapsedPlayBackTime(_ time: CMTime) {
        let crtTime = CMTimeGetSeconds(time)
        
        var nowPlayingInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
        nowPlayingInfo?[MPNowPlayingInfoPropertyElapsedPlaybackTime] = crtTime
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        
//        self.setupPlaybackRate(1.0)
    }
    
    // Update Playback Duration
    fileprivate func updatePlaybackDuration(_ time: CMTime) {
        let duration = CMTimeGetSeconds(time)
        
        var nowPlayerInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
        nowPlayerInfo![MPMediaItemPropertyPlaybackDuration] = duration
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayerInfo
    }
    
    fileprivate func setupPlaybackRate(_ value: Float) {
        var nowPlayerInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
        nowPlayerInfo![MPNowPlayingInfoPropertyPlaybackRate] = value
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayerInfo
    }
    

    fileprivate func setupNowPlayingInfo() {
        let nowPlayingInfoCenter = MPNowPlayingInfoCenter.default()
        guard let episode = currentEpisode else { return }

        var nowPlayingInfo = [String: Any]()
        
        nowPlayingInfo[MPMediaItemPropertyTitle] = episode.title
        nowPlayingInfo[MPMediaItemPropertyArtist] = episode.author

        nowPlayingInfoCenter.nowPlayingInfo = nowPlayingInfo
    }
    
    func replaceCurrentItem(with url: String) {
        guard let url = URL(string: url) else {return}
        self.avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer.replaceCurrentItem(with: avPlayerItem)
        
        self.setupAudioSession()
        self.setupRemoteControl()
        self.setupNowPlayingInfo()
        
        self.avPlayer.play()
        NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "playing"])
        
        self.observePlayerCurrentTime()
    }
    
    func play() {
        if self.avPlayer.timeControlStatus == .paused {
            avPlayer.play()
            NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "playing"])
            
//            self.setupPlaybackRate(1.0)
        }
        else {
            avPlayer.pause()
            NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "pause"])
            
//            self.setupPlaybackRate(0)
        }
    }
    
    func seekToCurrentTime(delta: Int64) {
        // paused player
        self.avPlayer.pause()
        NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "pause"])
        
        let fifteenSeconds = CMTimeMake(value: delta, timescale: 1)
        let seekTime = CMTimeAdd(self.currentTime, fifteenSeconds)
        self.seek(to: seekTime)
    }
    
    func seek(to time: CMTime) {
        self.avPlayer.seek(to: time)
        
        if avPlayer.rate < 1 {
            self.play()
        }
    }
    
    func adjustVolume(_ value: Float) {
//        NotificationCenter.default.post(name: .systemVolumeDidChange, object: nil, userInfo: ["AVSystemController_AudioVolumeNotificationParameter": value])
        self.volume = value
        self.avPlayer.volume = volume
    }
    
}
