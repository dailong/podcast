//
//  NotificationCenter.swift
//  Podcasts
//
//  Created by Dai Long on 12/6/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let didPlayAVPlayerItem = Notification.Name(rawValue: "didPlayAVPlayerItem")
    static let didPauseAVPlayerItem = Notification.Name(rawValue: "didPauseAVPlayerItem")
    static let didPausePlayAVPlayerItem = Notification.Name(rawValue: "didPausePlayAVPlayerItem")
    static let systemVolumeDidChange = Notification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification")
    static let didBeginDownload = Notification.Name(rawValue: "didBeginDownload")
}
