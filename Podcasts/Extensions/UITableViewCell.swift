//
//  UITableViewCell.swift
//  Podcasts
//
//  Created by Dai Long on 11/21/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell: ReusableView { }
extension UITableViewHeaderFooterView: ReusableView { }

extension UITableView {
    
    public func register<T: UITableViewCell>(_ cellClass: T.Type) {
        if cellClass.hasNib {
            self.register(cellClass.nib, forCellReuseIdentifier: cellClass.reuseIdentifier)
        }
        else {
            self.register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
        }
    }
    
    public func cellForRow<T: UITableViewCell>(at indexPath: IndexPath) -> T {
        guard let cell = cellForRow(at: indexPath) as? T else {
            fatalError("Count not downcast to \(String(describing: T.self))")
        }
        return cell
    }
    
    public func register<T: UITableViewHeaderFooterView>(aClass: T.Type) {
        register(aClass, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    public func dequeueResuableCell<T: UITableViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
    public func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        guard let view = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Could not dequeue header/footer view with identifier: \(T.reuseIdentifier)")
        }
        return view
    }
}
