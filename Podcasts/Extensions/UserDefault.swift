//
//  UserDefault.swift
//  Podcasts
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import Foundation

extension UserDefaults {
    func deleteEpisode(_ episode: Episode) {
        let downloadedEpisodes = self.downloadedEpisodes()
        let newEpisodes = downloadedEpisodes.filter { $0.title != episode.title && $0.author != episode.author }
        let fileName = URL(string: episode.fileUrl ?? "")?.lastPathComponent ?? ""
        if !fileName.isEmpty {
            let documentFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            do {
                try FileManager.default.removeItem(atPath: documentFolder.absoluteString + fileName)
            }
            catch let err {
                print("Cannot delete file:", err)
            }
        }
        do {
            let data = try JSONEncoder().encode(newEpisodes)
            self.set(data, forKey: downloadEpisodeKey)
        }
        catch let err {
            print("Failed to encode: ", err)
        }
    }
    
    func downloadEpisode(_ episode: Episode) {
        var downloadedEpisodes = self.downloadedEpisodes()
        
        // check duplicate
        let idx = downloadedEpisodes.firstIndex { $0.title == episode.title && $0.author == episode.author } ?? -1
        
        if idx != -1 {
            return
        }
        
        
        downloadedEpisodes.append(episode)
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(downloadedEpisodes)
            self.setValue(data, forKey: downloadEpisodeKey)
        } catch let err {
            print("Failed to encode:", err)
        }
        
    }
    
    func downloadedEpisodes() -> [Episode] {
        guard let data = self.value(forKey: downloadEpisodeKey) as? Data  else { return []}
        let decoder = JSONDecoder()
        do {
            return try decoder.decode([Episode].self, from: data)
        } catch let err {
            print("Failed to decode: ", err)
        }
        return []
    }
    
    
    func savedPodcasts() -> [Podcast] {
        let dataSaved = userDefault.data(forKey: podcastFavoriteKey)
        if let data = dataSaved {
            guard let podcasts = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Podcast] else {fatalError()}
            return podcasts
        }
        else {
            return []
        }
    }
    
    func deletePodcast(podcast: Podcast) {
        let savedPodcasts = userDefault.savedPodcasts()
        
        let newPodcasts = savedPodcasts.filter { p -> Bool in
            return p.collectionName != podcast.collectionName || p.artistName != podcast.artistName
        }
        
        let data = NSKeyedArchiver.archivedData(withRootObject: newPodcasts)
        userDefault.set(data, forKey: podcastFavoriteKey)
    }
}
