//
//  UICollectionView+Ex.swift
//  Podcasts
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import Foundation
import UIKit

public protocol ReusableView: class { }

extension ReusableView where Self: UIView {
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
    
    public static var hasNib: Bool {
        return Bundle.main.path(forResource: self.reuseIdentifier, ofType: "nib") != nil
    }
    
    public static var nib: UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: nil)
    }
}

extension UICollectionViewCell: ReusableView { }
//extension : ReusableView { }

extension UICollectionView {
    
    public func register<T: UICollectionViewCell>(_ cellClass: T.Type) {
        if cellClass.hasNib {
            self.register(cellClass.nib, forCellWithReuseIdentifier: cellClass.reuseIdentifier)
        }
        else {
            self.register(cellClass, forCellWithReuseIdentifier: cellClass.reuseIdentifier)
        }
    }
    
    public func cellForItem<T: UICollectionViewCell>(at indexPath: IndexPath) -> T {
        guard let cell = cellForItem(at: indexPath) as? T else {
            fatalError("Count not downcast to \(String(describing: T.self))")
        }
        return cell
    }
    
//    public func register<T: UITableViewHeaderFooterView>(aClass: T.Type) {
//        register(aClass, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
//    }
    
    public func dequeueResuableCell<T: UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
    
//    public func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
//        guard let view = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
//            fatalError("Could not dequeue header/footer view with identifier: \(T.reuseIdentifier)")
//        }
//        return view
//    }
}
