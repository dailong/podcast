//
//  DraggableDismissAnimationController.swift
//  Podcasts
//
//  Created by Dai Long on 12/12/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

class DraggableDismissAnimationController:NSObject, UIViewControllerAnimatedTransitioning {
    
    let interactionController: InteractiveTransition?
    var miniView: UIView!
    
    init(interactionController: InteractiveTransition?) {
        self.interactionController = interactionController
    }
    
    func animateDismissingInContext(_ transitionContext: UIViewControllerContextTransitioning, fromVC: UIViewController, toVC: UIViewController) {
        
        guard let maxiPlayerController = fromVC as? MaxiPlayerController else {fatalError()}
        guard let mainTabBarController = toVC as? MainTabBarController else {fatalError()}
        
        let snapshot = toVC.view.snapshotView(afterScreenUpdates: false)!
        
        snapshot.frame = CGRect(x: 15.0, y: statusBarHeight(), width: UIScreen.main.bounds.size.width - 2*15.0, height: UIScreen.main.bounds.size.height - 2*statusBarHeight())
        snapshot.layer.cornerRadius = 15.0
        snapshot.layer.masksToBounds = true
        
        let dimLayer = UIView(frame: CGRect.zero)
        dimLayer.translatesAutoresizingMaskIntoConstraints = false
        dimLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        snapshot.addSubview(dimLayer)
        
        NSLayoutConstraint.activate([
            dimLayer.leadingAnchor.constraint(equalTo: snapshot.leadingAnchor, constant: 0),
            dimLayer.trailingAnchor.constraint(equalTo: snapshot.trailingAnchor, constant: 0),
            dimLayer.topAnchor.constraint(equalTo: snapshot.topAnchor, constant: 0),
            dimLayer.bottomAnchor.constraint(equalTo: snapshot.bottomAnchor, constant: 0),
        ])
        
        let fromRect = transitionContext.initialFrame(for: fromVC)

        let screenHeight = UIScreen.main.bounds.height
        let screenWidth = UIScreen.main.bounds.width
        
        let tabBarHeight = mainTabBarController.tabBar.frame.height
        
        let tabBarImageView = UIImageView(frame: CGRect(x: 0, y: screenHeight,
                                                        width: screenWidth, height: tabBarHeight))
        tabBarImageView.image = mainTabBarController.tabBar.makeSnapshot()
        
        let container = transitionContext.containerView
        
        container.addSubview(snapshot)
        container.addSubview(fromVC.view)
        container.addSubview(tabBarImageView)
        
        let animOptions: UIView.AnimationOptions = [.curveLinear, .allowAnimatedContent]
        let interval = transitionDuration(using: transitionContext)
        
        UIView.animate(withDuration: interval, delay: 0, options: animOptions, animations: {
            
            snapshot.frame = CGRect(x: 0, y: 0, width: fromRect.width, height: screenHeight)
            dimLayer.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            fromVC.view.frame = maxiPlayerController.sourceView.originatingFrameInWindow

            
            maxiPlayerController.animateImageLayerOut { _ in
                let episode = maxiPlayerController.episode
                mainTabBarController.nextViewController = nil
                mainTabBarController.reCreateMaxiPlayerController(with: episode)
            }
            
            maxiPlayerController.animateCoverImageOut()
            
            tabBarImageView.frame.origin.y = screenHeight - tabBarHeight
            
        }) { (finished) in
            
            snapshot.removeFromSuperview()
            dimLayer.removeFromSuperview()
            tabBarImageView.removeFromSuperview()
            
            if transitionContext.transitionWasCancelled {
                transitionContext.completeTransition(false)
            } else {
                transitionContext.completeTransition(true)
            }
        }
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to) else {return}
        
        animateDismissingInContext(transitionContext, fromVC: fromVC, toVC: toVC)
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionContext!.isInteractive ? 0.4 : 0.4
    }
}
