//
//  Appearance.swift
//  Podcasts
//
//  Created by Dai Long on 11/26/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var startAnimColor: UIColor {
        return UIColor.white.withAlphaComponent(0.8)
    }
    
    static var endAnimColor: UIColor {
        return .white
    }
}
