//
//  SongPlayerController.swift
//  Podcasts
//
//  Created by Dai Long on 11/26/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

protocol SongPlayerControllerDelegate: class {
    func shrinkExpandImageView(isShrink: Bool)
}

class SongPlayerController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var currentTimeSlider: UISlider!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var currentTimeLabel: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var skipBackButton: UIButton!
    @IBOutlet weak var skipAheadButton: UIButton!
    
    @IBOutlet weak var volumeSlider: MPVolumeView!
    
    var episode: Episode! {
        didSet {
//            self.titleLabel.text = episode.title
//            self.authorLabel.text = episode.author
        }
    }
    
    weak var delegate: SongPlayerControllerDelegate?
    
    // MARK: - View life cycles
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(onHandlePausePlay(_:)), name: Notification.Name.didPausePlayAVPlayerItem, object: nil)
        
        AVPlayerControl.shared.observePlayerCurrentTime()

        
        if AVPlayerControl.shared.avPlayer.timeControlStatus == .playing {
            self.playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            self.delegate?.shrinkExpandImageView(isShrink: false)
        }
        else {
            self.playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            self.delegate?.shrinkExpandImageView(isShrink: true)
        }
        
        self.titleLabel.text = currentEpisode.title
        self.authorLabel.text = currentEpisode.author
        
        // Setup time label
        self.setupTimeLabel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        AVPlayerControl.shared.delegate = self
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        self.setupCurrentTimeSlider()
    }

    
    deinit {
        print("SongPlayerViewController was deinited")

        NotificationCenter.default.removeObserver(self)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }

    // MARK: - IBAction
    @IBAction func playPauseAction(_ sender: Any) {
        AVPlayerControl.shared.play()
    }

    @IBAction func handleCurrentTimeSliderValueChanged(_ sender: Any) {
        let duration = AVPlayerControl.shared.durationTime
        let percentage = Float64(self.currentTimeSlider.value)
        let newCrtTime = percentage * CMTimeGetSeconds(duration)
        let seekTime = CMTimeMakeWithSeconds(newCrtTime, preferredTimescale: 1)

        AVPlayerControl.shared.seek(to: seekTime)
    }
    
    @objc private func onHandlePausePlay(_ notification: Notification) {
        print("onHandlePausePlay")
        
        guard let userInfo = notification.userInfo else { return }
        guard let state = userInfo["state"] as? String else { return }
        if state == "playing" {
            self.playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
            self.delegate?.shrinkExpandImageView(isShrink: false)
        }
        else {
            self.playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
            self.delegate?.shrinkExpandImageView(isShrink: true)
        }
    }
    
    @IBAction func skipBackAction(_ sender: Any) {
        AVPlayerControl.shared.seekToCurrentTime(delta: -15)
    }
    
    @IBAction func skipAheadAction(_ sender: Any) {
        AVPlayerControl.shared.seekToCurrentTime(delta: 15)
    }
    
    // MARK: - Setup View
    func setupCurrentTimeSlider() {
        self.currentTimeSlider.value = 0
    }
    
    func setupTimeLabel() {
        let durationTime = AVPlayerControl.shared.durationTime
        let crtTime = AVPlayerControl.shared.currentTime
        
        self.currentTimeLabel.text = crtTime.toString()
        self.remainingTimeLabel.text = durationTime.toString()
    }
    
    // MARK: - Method
    
}


extension SongPlayerController: AVPlayerControlDelegate {
    func observePlayerCurrentTime(_ time: CMTime) {
        // Current time
        self.currentTimeLabel.text = time.toString()

        // Remaining Time
        let durationTime = AVPlayerControl.shared.durationTime
        let remainingTime = durationTime - time
        self.remainingTimeLabel.text = "-" + remainingTime.toString()

        // Update Current Time Silder
        self.updateCurrentTimeSlider()
    }
    
    private func updateCurrentTimeSlider() {
        let currentTime = CMTimeGetSeconds(AVPlayerControl.shared.currentTime)
        let durationTime = CMTimeGetSeconds(AVPlayerControl.shared.durationTime)
        let percentage = currentTime/durationTime
        self.currentTimeSlider.value = Float(percentage)
    }
}
