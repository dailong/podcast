//
//  MaxiPlayerController.swift
//  Podcasts
//
//  Created by Dai Long on 11/24/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

protocol MaxiPlayerSourceProtocol: class {
    var originatingFrameInWindow: CGRect { get }
    var originatingCoverImageView: UIImageView { get }
}

class MaxiPlayerController: UIViewController{//}, SongSubscriber {
    
    var rootViewController: MainTabBarController!
    var dismissInteractor: InteractiveTransition!
    
    // MARK: - Properties
    let primaryDuration = 0.5
    let backingImageEdgeInset: CGFloat = 15.0
    let cardCornerRadius: CGFloat = 10
//    var currentSong: Song?
    weak var sourceView: MaxiPlayerSourceProtocol!
    
    //scroller
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stretchySkirt: UIView!
    //cover image
    @IBOutlet weak var coverImageContainer: UIView!
    @IBOutlet weak var coverArtImage: UIImageView!
    @IBOutlet weak var dismissChevron: UIButton!
    
    //cover image constraints
    @IBOutlet weak var coverImageLeading: NSLayoutConstraint!
    @IBOutlet weak var coverImageTop: NSLayoutConstraint!
    @IBOutlet weak var coverImageBottom: NSLayoutConstraint!
    @IBOutlet weak var coverImageHeight: NSLayoutConstraint!
    
    //backing image
    var backingImage: UIImage?
    
    var tabBarImage: UIImage?
    
    //lower module constraints
    @IBOutlet weak var lowerModuleTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var songPlayerView: UIView!
    
    
    var episode: Episode! {
        didSet {
//            self.coverArtImage.sd_setImage(with: URL(string: episode.imageURL!))
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        modalPresentationCapturesStatusBarAppearance = true //allow this VC to control the status bar appearance
        modalPresentationStyle = .overFullScreen //dont dismiss the presenting view controller when presented
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dismissInteractor = InteractiveTransition(viewController: self)
        
        scrollView.delegate = self
        scrollView.contentInsetAdjustmentBehavior = .never //dont let Safe Area insets affect the scroll view
        
        coverImageContainer.layer.cornerRadius = cardCornerRadius
        coverImageContainer.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
        scrollView.layer.cornerRadius = cardCornerRadius
        scrollView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let showSongPlayerController = "showSongPlayerController"
        if let songPlayerController = segue.destination as? SongPlayerController, segue.identifier == showSongPlayerController {
//            songPlayerController.episode = self.episode
            songPlayerController.delegate = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        coverArtImage.image = sourceView.originatingCoverImageView.image

    }
    
    deinit {
        print("MaxiPlayerController was deinited")
    }
    
    
}

// MARK: - IBActions
extension MaxiPlayerController {
    
    @IBAction func dismissAction(_ sender: Any) {
        self.rootViewController.disableInteractivePlayerTransitioning = true
        self.dismiss(animated: true) {
            [unowned self] in
            self.rootViewController.disableInteractivePlayerTransitioning = false
            self.rootViewController = nil
        }
    }
    
}

//Image Container animation.
extension MaxiPlayerController {
    private var imageLayerInsetForOutPosition: CGFloat {
        let imageFrame = view.convert(sourceView.originatingFrameInWindow, to: view)
        let inset = imageFrame.minY - backingImageEdgeInset
        return inset
    }
    
    func configureImageLayerInStartPosition() {
        coverImageContainer.backgroundColor = UIColor.startAnimColor
        dismissChevron.alpha = 0
        coverImageContainer.layer.cornerRadius = 0
        view.layoutIfNeeded()
    }
    
    func animateImageLayerIn() {
        UIView.animate(withDuration: primaryDuration / 4.0) {
            self.coverImageContainer.backgroundColor = UIColor.endAnimColor
        }
        
        UIView.animate(withDuration: primaryDuration, delay: 0, options: [.curveEaseIn], animations: {
            self.dismissChevron.alpha = 1
            self.coverImageContainer.layer.cornerRadius = self.cardCornerRadius
            self.view.layoutIfNeeded()
        })
    }
    
    func animateImageLayerOut(completion: @escaping ((Bool) -> Void)) {
        
        UIView.animate(withDuration: primaryDuration / 4.0, delay: primaryDuration , options: [.curveEaseOut], animations: {
            self.coverImageContainer.backgroundColor = UIColor.startAnimColor
        }, completion: { finished in
            completion(finished)
        })
        
        UIView.animate(withDuration: primaryDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.dismissChevron.alpha = 0
            self.coverImageContainer.layer.cornerRadius = 0
            self.view.layoutIfNeeded()
        })
    }
}

//cover image animation
extension MaxiPlayerController {
    
    func configureCoverImageInStartPosition() {
        let originatingImageFrame = sourceView.originatingCoverImageView.frame
        coverImageHeight.constant = originatingImageFrame.height
        coverImageLeading.constant = originatingImageFrame.minX
        coverImageTop.constant = originatingImageFrame.minY
        coverImageBottom.constant = originatingImageFrame.minY
        self.view.layoutIfNeeded()
    }
    
    func animateCoverImageIn() {
        let coverImageEdgeContraint: CGFloat = 32
        let endHeight = coverImageContainer.bounds.width - coverImageEdgeContraint * 2
        
        UIView.animate(withDuration: primaryDuration, delay: 0, options: [.curveEaseIn], animations: {
            self.coverImageHeight.constant = endHeight
            self.coverImageLeading.constant = coverImageEdgeContraint
            self.coverImageTop.constant = coverImageEdgeContraint
            self.coverImageBottom.constant = coverImageEdgeContraint
            self.view.layoutIfNeeded()
        })
    }
    
    func animateCoverImageOut() {
        UIView.animate(withDuration: primaryDuration, delay: 0, options: [.curveEaseOut], animations: {
            self.configureCoverImageInStartPosition()
            self.view.layoutIfNeeded()
        })
    }
}

// SongPlayerController Delegate
extension MaxiPlayerController: SongPlayerControllerDelegate {
    func shrinkExpandImageView(isShrink: Bool) {
        if isShrink {
            shrinkImageView()
        }
        else {
            expandImageView()
        }
    }
    
    func expandImageView() {
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
            self.coverArtImage.transform = .identity
        }, completion: nil)
    }
    
    func shrinkImageView() {
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseIn, animations: {
            let scale: CGFloat = 0.78
            self.coverArtImage.transform = CGAffineTransform(scaleX: scale, y: scale)
        }, completion: nil)
    }
}

extension MaxiPlayerController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let translation = self.scrollView.panGestureRecognizer.translation(in: view)
        let velocity = self.scrollView.panGestureRecognizer.velocity(in: view)
//        print("Scroll ", translation.y)
//        print("Velocity", velocity)
        if translation.y > 350 || velocity.y > 2000.0 {
            self.dismissChevron.sendActions(for: .touchUpInside)
        }
    }
}
