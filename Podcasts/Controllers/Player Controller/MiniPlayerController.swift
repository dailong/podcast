//
//  MiniPlayerController.swift
//  Podcasts
//
//  Created by Dai Long on 12/4/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import SDWebImage
import MediaPlayer
import AVKit

protocol MiniPlayerDelegate: class {
    func expandPlayer(_ episode: Episode?)
}

class MiniPlayerController: UIViewController {

    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var coverImageAvt: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var skipAheadButton: UIButton!
    
    
    weak var tabbarController: MainTabBarController?
    weak var delegate: MiniPlayerDelegate?
    
    var episode: Episode! {
        didSet {
            // Configure UI
            coverImageAvt.sd_setImage(with: URL(string: episode.imageURL ?? "")) { (image, error, _, _) in
//                currentEpisode.artWork = image
                var nowPlayerInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
                
                let mediaPropertyArtwork = MPMediaItemArtwork(boundsSize: image!.size) { (_) -> UIImage in
                    return image!
                }
                
                nowPlayerInfo?[MPMediaItemPropertyArtwork] = mediaPropertyArtwork
                
                MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayerInfo
                
            }
//            coverImageAvt.sd_setImage(with: URL(string: episode.imageURL ?? ""))
            titleLabel.text = episode.title
            
            
//            AVPlayerControl.shared.observePlayerCurrentTime()
            var url = episode.streamUrl
            if let fileUrl = episode.fileUrl {
                let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                let urlComponent = URL(string: fileUrl)
                let fileName = urlComponent?.lastPathComponent ?? ""
                url = documentUrl.absoluteString + fileName
            }
            
            
            
            
            AVPlayerControl.shared.replaceCurrentItem(with: url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Play AVPlayer Observer
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(onHandlePausePlay(_:)), name: Notification.Name.didPausePlayAVPlayerItem, object: nil)
        
        // Interruption Observer
        notificationCenter.addObserver(self, selector: #selector(onHandleInterruption(_:)), name: AVAudioSession.interruptionNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        AVPlayerControl.shared.delegate = self

        coverImageAvt.layer.cornerRadius = 5.0
        coverImageAvt.layer.masksToBounds = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func tapAction(_ sender: Any) {
        // Expand Max Player
        if let episode = self.episode {
            delegate?.expandPlayer(episode)
        }
    }
    
    
    @objc private func onHandleInterruption(_ notification: Notification) {
//        print("onHandleInterruption")
        
        guard let userInfo = notification.userInfo, let typeValue = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt, let type = AVAudioSession.InterruptionType(rawValue: typeValue) else {fatalError()}
        
        if type == .began {
            print("Interruption was begin")
            AVPlayerControl.shared.avPlayer.pause()
            NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "pause"])
        }
        else if type == .ended {
            print("Interruption was end...")
//            AVPlayerControl.shared.avPlayer.play()
//            NotificationCenter.default.post(name: .didPausePlayAVPlayerItem, object: nil, userInfo: ["state": "playing"])
        }
    }
    
    @objc private func onHandlePausePlay(_ notification: Notification) {
        print("onHandlePausePlay in miniPlayerController")
        
        guard let userInfo = notification.userInfo else { return }
        guard let state = userInfo["state"] as? String else { return }
        if state == "playing" {
            self.playButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
        }
        else {
            self.playButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        }
    }
    
    // MARK: - Action
    
    @IBAction func pausePlayAction(_ sender: Any) {
        AVPlayerControl.shared.play()
    }
    @IBAction func skipAheadAction(_ sender: Any) {
        AVPlayerControl.shared.seekToCurrentTime(delta: 15)
    }
}

extension MiniPlayerController: MaxiPlayerSourceProtocol {
    var originatingFrameInWindow: CGRect {
        return view.convert(view.frame, to: nil)
    }
    
    var originatingCoverImageView: UIImageView {
        return self.coverImageAvt
    }
}


//extension MiniPlayerController: AVPlayerControlDelegate {
//    func observePlayerCurrentTime(_ time: CMTime) {
//
//        let durationTime = AVPlayerControl.shared.avPlayer.currentItem?.duration
//        let remainingTime =  CMTimeGetSeconds(durationTime!)
//
//        // Update Current Time Silder
//        updateTime(duration: remainingTime)
//    }
//
//    // Update time for command center
//    func updateTime(duration: Float64) {
//        var nowPlayerInfo = MPNowPlayingInfoCenter.default().nowPlayingInfo
//
////        nowPlayerInfo![MPNowPlayingInfoPropertyElapsedPlaybackTime] = currentTime
//        nowPlayerInfo![MPMediaItemPropertyPlaybackDuration] = duration
//
//        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayerInfo
//    }
//}
