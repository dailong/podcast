//
//  FavoritesController.swift
//  Podcasts
//
//  Created by Long Vu on 5/27/19.
//  Copyright © 2019 Dai Long. All rights reserved.
//

import UIKit

class FavoritesController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    // MARK: - Properties
    var podcasts: [Podcast] = userDefault.savedPodcasts() {
        didSet {
            self.collectionView.reloadData()
            UIApplication.mainTabBarController()?.viewControllers?[0].tabBarItem.badgeValue = nil
        }
    }
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.podcasts = userDefault.savedPodcasts()
    }
    
    func setupCollectionView() {
        collectionView.backgroundColor = .white
        
        // register cell
        collectionView.register(FavoritePodcastCell.self)
        
        // add gesture
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        collectionView.addGestureRecognizer(longPressGesture)
    }
    
    // MARK: - Helper Methods
    @objc private func handleLongPress(_ sender: UILongPressGestureRecognizer) {
        let selectedLocation = sender.location(in: collectionView)
        guard let selectedIdxPath = collectionView.indexPathForItem(at: selectedLocation) else {return}
        
        let alert = UIAlertController(title: "Remove this Podcast?", message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { _ in
            let selectedPodcast = self.podcasts[selectedIdxPath.item]
            self.podcasts.remove(at: selectedIdxPath.item)
            self.collectionView.deleteItems(at: [selectedIdxPath])
            // remove in user default
            userDefault.deletePodcast(podcast: selectedPodcast)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Datasource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return podcasts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: FavoritePodcastCell = collectionView.dequeueResuableCell(forIndexPath: indexPath)

        cell.podcast = podcasts[indexPath.row]
        
        return cell
    }
    
    // MARK: - Delegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let podcast  = self.podcasts[indexPath.item]
        
        let episodesController = EpisodesController()
        episodesController.podcast = podcast
        
        navigationController?.pushViewController(episodesController, animated: true)
        collectionView.deselectItem(at: indexPath, animated: true)
    }

    
    // MARK: - Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (UIScreen.main.bounds.width - 16*3)/2
        return CGSize(width: width, height: width + 64.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16.0, left: 16.0, bottom: 16.0, right: 16.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
