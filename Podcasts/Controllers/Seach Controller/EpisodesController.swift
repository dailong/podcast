//
//  EpisodesController.swift
//  Podcasts
//
//  Created by Dai Long on 11/22/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit
import FeedKit

class EpisodesController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView = UITableView(frame: CGRect.zero)

    // MARK: - Properties
    var podcast: Podcast! {
        didSet {
            navigationItem.title = podcast.collectionName
        }
    }
    
    var episodes = [Episode]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.setupTableView()
        
        self.setupNavigationBarButtons()
        
        APIService.shared.fetchEpisodes(self.podcast.feedUrl) { episodes in
            self.episodes = episodes
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    deinit {
        print("EpisodesController was deinited")
    }

    // MARK: - Helper Method
    private func setupTableView() {
        self.tableView.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.width, height: view.frame.height)
        self.view.addSubview(self.tableView)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(EpisodeCell.self)
        self.tableView.tableFooterView = UIView()
    }
    
    private func setupNavigationBarButtons() {
        // check podcast is favorited or not yet
        let savedPodcasts = userDefault.savedPodcasts()
        
        let hasFavorited = savedPodcasts.firstIndex { $0.collectionName == podcast.collectionName && $0.artistName == podcast.artistName}
        
        if hasFavorited != nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "heart"), style: .done, target: self, action: #selector(handleUnfavorite))
        }
        else {
            navigationItem.rightBarButtonItem =
                UIBarButtonItem(title: "Favorite", style: .plain, target: self, action: #selector(handleSaveFavorite))
//                UIBarButtonItem(title: "Fetch", style: .plain, target: self, action: #selector(handleFetchSavedFavorite))
            
        }
        
    }
    
    @objc private func handleUnfavorite() {
        print("unfavorite")
        userDefault.deletePodcast(podcast: self.podcast)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Favorite", style: .plain, target: self, action: #selector(handleSaveFavorite))
    }
    
//    let podcast
    @objc private func handleSaveFavorite() {
        print("saving podcast into Userdefault...")
        
        guard let podcast = self.podcast else {fatalError()}
        
        // get list of saved podcasts
        var podcasts = userDefault.savedPodcasts()
        podcasts.append(podcast)
        
        let newData = NSKeyedArchiver.archivedData(withRootObject: podcasts)
        
        userDefault.setValue(newData, forKey: podcastFavoriteKey)
        
        showBadgeHighlight()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "heart"), style: .done, target: self, action: #selector(handleUnfavorite))
    }
    
    @objc private func handleFetchSavedFavorite() {
        print("fetching podcast from UserDefault...")
        
        let podcasts = userDefault.savedPodcasts()
        podcasts.forEach { podcast in
            print("Name: ", podcast.collectionName)
        }
        
    }
    
    private func showBadgeHighlight() {
        UIApplication.mainTabBarController()?.viewControllers?[0].tabBarItem.badgeValue = "New"
    }
    
    
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.episodes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: EpisodeCell = tableView.dequeueResuableCell(forIndexPath: indexPath)

        let episode = self.episodes[indexPath.row]
        
        // Configure the cell...
        cell.episode = episode
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let downloadAction = UITableViewRowAction(style: .normal, title: "Download") { (_, indexPath) in
//            print("Download at: ", indexPath)
            let episode = self.episodes[indexPath.row]
            userDefault.downloadEpisode(episode)
            APIService.shared.downloadEpisode(episode)
        }
        return [downloadAction]
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let episode = self.episodes[indexPath.row]
        
        guard let mainTabBarController = tabBarController as? MainTabBarController else {
            assertionFailure("Not have tabbar")
            return
        }
        
        // global
        currentEpisode = episode
        
        mainTabBarController.miniPlayerController.episode = episode
        mainTabBarController.reCreateMaxiPlayerController(with: episode)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.color = .darkGray
        activityIndicator.startAnimating()
        return activityIndicator
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return self.episodes.count > 0 ? 0 : 200.0
    }
}
