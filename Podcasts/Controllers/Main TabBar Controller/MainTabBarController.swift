//
//  MainTabBarController.swift
//  Podcasts
//
//  Created by Dai Long on 11/20/18.
//  Copyright © 2018 Dai Long. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    var disableInteractivePlayerTransitioning = false
    
    let containerView = UIView()
    var miniPlayerController: MiniPlayerController!
    
    var presentInteractor: InteractiveTransition!
    
//    var currentVC: UIViewController?
    var nextViewController: MaxiPlayerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.tintColor = .purple
        
        let favoritesController = self.generateNavigationController(with: FavoritesController(collectionViewLayout: UICollectionViewFlowLayout.init()), title: "Favorites", image: #imageLiteral(resourceName: "favorites"))
        let searchController = self.generateNavigationController(with: PodcastsSearchController(), title: "Search", image: #imageLiteral(resourceName: "search"))
        let downloadsController = self.generateNavigationController(with: DownloadsController(style: .plain), title: "Downloads", image: #imageLiteral(resourceName: "download"))
        
        viewControllers = [
            favoritesController,
            searchController,
            downloadsController
        ]
        
        self.setupMiniPlayer()
        
        miniPlayerController.delegate = self
        
        self.reCreateMaxiPlayerController(with: self.miniPlayerController.episode)
    }
    
    func reCreateMaxiPlayerController(with episode: Episode?) {
        guard currentEpisode != nil else {
            return
        }
        
        guard self.nextViewController == nil else {return}
        self.nextViewController = ViewController.maxiPlayerController()
        
        self.nextViewController.rootViewController = self
        
        self.nextViewController.transitioningDelegate = self
        

        
        self.presentInteractor = InteractiveTransition(viewController: self, presentVC: nextViewController)
        
        nextViewController.sourceView = self.miniPlayerController
        
        nextViewController.episode = episode
        
        nextViewController.tabBarImage = tabBar.makeSnapshot()
        nextViewController.backingImage = view.makeSnapshot()
    }
    
    fileprivate func setupMiniPlayer() {
        view.addSubview(self.containerView)
        
        self.containerView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor, constant: 0),
            containerView.heightAnchor.constraint(equalToConstant: tabBar.frame.height + 10)
        ])
        
        let controller = ViewController.miniPlayerController()
        
        self.miniPlayerController = controller
        controller.tabbarController = self
        
        self.containerView.addSubview(controller.view)
        
        controller.view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            controller.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor, constant: 0),
            controller.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor, constant: 0),
            controller.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor, constant: 0),
            controller.view.topAnchor.constraint(equalTo: self.containerView.topAnchor, constant: 0)
        ])
    }
    
    // MARK: - Helper Functions
    
    fileprivate func generateNavigationController(with rootViewController: UIViewController, title: String, image: UIImage) -> UIViewController {
        let navController = UINavigationController(rootViewController: rootViewController)
        navController.navigationBar.prefersLargeTitles = true
        rootViewController.navigationItem.title = title
        navController.tabBarItem.title = title
        navController.tabBarItem.image = image
        return navController
    }
}

extension MainTabBarController: MiniPlayerDelegate {
    func expandPlayer(_ episode: Episode?) {
        if self.nextViewController == nil {
            reCreateMaxiPlayerController(with: episode)
        }
        self.disableInteractivePlayerTransitioning = true
        self.present(nextViewController, animated: true) { [unowned self] in
            self.disableInteractivePlayerTransitioning = false
        }
    }
}


extension MainTabBarController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    
        let presentController = DraggablePresentedAnimationController()
        presentController.initialY = miniPlayerController.originatingFrameInWindow.minY
        
        return presentController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let maxiPlayerController = dismissed as? MaxiPlayerController else {fatalError()}
        return DraggableDismissAnimationController(interactionController: maxiPlayerController.dismissInteractor)
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard !disableInteractivePlayerTransitioning else {return nil}
        return presentInteractor
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        guard !disableInteractivePlayerTransitioning else {return nil}
        guard let animator = animator as? DraggableDismissAnimationController,
            let interactionController = animator.interactionController
            else { fatalError() }
        return interactionController
    }
}
